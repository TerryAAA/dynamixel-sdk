/**
  ******************************************************************************
  * @file buffer.cpp
	*
	* Code of uart recive and transmit buffer, usart1.
	*
  * @author 健宝 <JohnLiuljj@163.com>
  ******************************************************************************
  */
#include "buffer.h"


extern uint8_t rx_buffer[1];
extern std::vector<uint8_t>  buffer;


void BufferClear(void){
	buffer.clear();
}

void BufferRead(uint8_t *packet, int length){
	
	for(int i = 0; i < length; i++){
		packet[i] = buffer[i];
	}
	buffer.erase(buffer.begin(), buffer.begin() + length);
}

void BufferWrite(uint8_t *packet, int length){

	HAL_UART_Transmit(&huart1, (uint8_t *)packet, length, 0xffff);
}


uint8_t BufferAvailable(void){
	
	return buffer.size();
}



