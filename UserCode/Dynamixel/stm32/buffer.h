/**
  ******************************************************************************
  * @file buffer.h
	*
	* Heder of buffer.cpp.
	*
  * @author 健宝 <JohnLiuljj@163.com>
  ******************************************************************************
  */
#ifndef __BUFFER_H
#define __BUFFER_H

#include "usart.h"
#include "irq.h"
#include <vector>


void BufferRead(uint8_t *packet, int length);
void BufferWrite(uint8_t *packet, int length);
uint8_t BufferAvailable(void);
void BufferClear(void);
void BufferStart(void);

#endif
