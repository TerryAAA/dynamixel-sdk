/**
  ******************************************************************************
  * @file stm32_hander.h
	*
	* Heder of stm32_hander.cpp.
	*
  * @author 健宝 <JohnLiuljj@163.com>
  ******************************************************************************
  */
#ifndef __STM32_HANDER_H 
#define __STM32_HANDER_H




#ifdef __cplusplus
extern "C" {
#endif
	
void DynamixelTest(void);
	
#ifdef __cplusplus
}
#endif



#endif
