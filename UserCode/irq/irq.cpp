/**
  ******************************************************************************
  * @file irq.cpp
	*
	* The file contain the irq works of this stm32 chip.
	*
  * @author 健宝 <JohnLiuljj@163.com>
  ******************************************************************************
  */
#include "irq.h"
#include "stdio.h" 
#include "usart.h"
#include "buffer.h"
#include "spi.h"


uint8_t rx_buffer[1];
std::vector<uint8_t>  buffer;

uint8_t aTxBuffer[5] = {0};
uint8_t aRxBuffer[5] = {0};


void usart1StartIT(void){

	HAL_UART_Receive_IT(&huart1, (uint8_t *)rx_buffer, 1);
}


void spi1StartIT(void){

	HAL_SPI_TransmitReceive_IT(&hspi1, (uint8_t *)aTxBuffer,(uint8_t *)aRxBuffer, 5);
}


void HAL_UART_RxCpltCallback(UART_HandleTypeDef *huart){

	if(huart->Instance==USART1){
		HAL_UART_Receive_IT(&huart1, (uint8_t *)rx_buffer, 1);
		buffer.push_back(rx_buffer[0]);
	}
	
	
}



void HAL_SPI_TxRxCpltCallback(SPI_HandleTypeDef *hspi){
	
	if(hspi->Instance==SPI1){
		static uint16_t a;
		HAL_SPI_TransmitReceive_IT(&hspi1, (uint8_t *)aTxBuffer,(uint8_t *)aRxBuffer, 5);
		a++;
		if(aRxBuffer[0] == 0x55){
			//a = (aRxBuffer[1] << 8) + aRxBuffer[2];
			aTxBuffer[0] = 0;
			aTxBuffer[1] = 0;
			aTxBuffer[2] = 0;
			aTxBuffer[3] = (uint8_t)(a >> 8);
			aTxBuffer[4] = (uint8_t)(a & 0x00ff);
		}
	}
}







