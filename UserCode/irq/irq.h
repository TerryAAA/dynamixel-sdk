/**
  ******************************************************************************
  * @file irq.h
	*
	* Heder of irq.cpp.
	*
  * @author 健宝 <JohnLiuljj@163.com>
  ******************************************************************************
  */
#ifndef __IRQ_H
#define __IRQ_H



#ifdef __cplusplus
extern "C" {
#endif
	
void usart1StartIT(void);
void spi1StartIT(void);
	
#ifdef __cplusplus
}
#endif




#endif
