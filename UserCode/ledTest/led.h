/**
  ******************************************************************************
  * @file led.h
	*
	* Heder of led.cpp.
	*
  * @author 健宝 <JohnLiuljj@163.com>
  ******************************************************************************
  */
#ifndef __LED_H
#define __LED_H

#ifdef __cplusplus
extern "C" {
#endif
	
void LedTestrun(void);
	
#ifdef __cplusplus
}
#endif



#endif
