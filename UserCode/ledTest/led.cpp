/**
  ******************************************************************************
  * @file led.cpp
	*
	* Code for driving led, PE13, pull down.
	*
  * @author 健宝 <JohnLiuljj@163.com>
  ******************************************************************************
  */
#include "led.h"
#include "gpio.h"
#include "stdio.h" 

class LedTest{
public:
	LedTest(bool state);
	void ledOn();
	void ledOff();
  bool ledState();
  void ledBlink();
  void ledToggle();
private:
	bool ledflag;
};


LedTest::LedTest(bool state){
	
	if(state == 0){
		ledflag = 0;
		HAL_GPIO_WritePin(GPIOC, GPIO_PIN_13, GPIO_PIN_SET);
	}else{
		ledflag = 1;
		HAL_GPIO_WritePin(GPIOC, GPIO_PIN_13, GPIO_PIN_RESET);
	}
}


void LedTest::ledOn(){
	
	HAL_GPIO_WritePin(GPIOC, GPIO_PIN_13, GPIO_PIN_RESET);
	ledflag = 1;
}


void LedTest::ledOff(){
	
	HAL_GPIO_WritePin(GPIOC, GPIO_PIN_13, GPIO_PIN_SET);
	ledflag = 0;
}


bool LedTest::ledState(){
	
	return ledflag;
}


void LedTest::ledToggle(){
	if(ledflag == 0){
		ledflag = 1;
		HAL_GPIO_WritePin(GPIOC, GPIO_PIN_13, GPIO_PIN_RESET);
	}else{
		ledflag = 0;
		HAL_GPIO_WritePin(GPIOC, GPIO_PIN_13, GPIO_PIN_SET);
	}
}


void LedTest::ledBlink(){
	ledToggle();
	HAL_Delay(100);
}


void LedTestrun(void){
	static LedTest *led1;
	static uint8_t i = 0;
	if(i == 0){
		led1 = (new LedTest(0));
		i = 1;
	}
	
	led1->ledBlink();
}





